import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ValidatorsService } from '../../services/validators.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email = new FormControl('', [ Validators.required, this.regx.email ]);
  public password = new FormControl('', [ Validators.required ]);
  public hidePass: boolean = true;
  public loader: boolean = false;
  public disableClick: boolean = false;

  constructor(
    private regx: ValidatorsService
  ) { }

  ngOnInit() {
  }

}
