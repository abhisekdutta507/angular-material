import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ValidatorsService } from '../../services/validators.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  public email = new FormControl('', [ Validators.required, this.regx.email ]);
  public password = new FormControl('', [ Validators.required ]);
  public hidePass: boolean = true;
  public loader: boolean = false;
  public disableClick: boolean = false;
  public gender: string;
  public genderList: string[] = ['Female', 'Male'];

  constructor(
    private regx: ValidatorsService
  ) { }

  ngOnInit() {
  }

}
